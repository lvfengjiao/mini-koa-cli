import path from "path";
import fs from "fs";
import { createEntryfile } from "../lib/createEntryfile.js";
import { question } from "../lib/question.js";
import { install } from '../lib/install.js'

let answers = await question();
let middlewares = answers.middlewares.reduce((sum, i) => {
  if (i.includes("Static")) {
    sum.static = true;
  }
  if (i.includes("Cors")) {
    sum.cors = true;
  }
  if (i.includes("Router")) {
    sum.router = true;
  }
  return sum;
}, {});
answers = {
  ...answers,
  middlewares
};

const config = Object.assign(
  {
    middlewares: {
      static: false,
      cors: false,
      router: true,
    },
  },
  answers
);

fs.mkdirSync(getRootPath());
fs.writeFileSync(
  path.resolve(`${getRootPath()}`, "index.js"),
  await createEntryfile("index", config),
  {}
);
fs.writeFileSync(
  path.resolve(`${getRootPath()}`, "package.json"),
  await createEntryfile("package", config),
  {}
);
install(config);
function getRootPath() {
  return "hello";
} // const has temp dead scope/zone


