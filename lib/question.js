import inquirer from 'inquirer'

export const question = async () => {
    return await inquirer.prompt([{
        type:'input',
        name: 'packageName',
        message: 'set Package Name',
        validate(val){
            if(val) return true;
            return 'please enter package name'
        }
    },{
        type:'input',
        name: 'port',
        message: 'set port',
        validate(val){
            if(val) return true;
            return 'please enter port'
        }
    },
    {
        type:'checkbox',
        name: 'middlewares',
        message: 'set Package Name',
        choices: [{
            name: 'koaCors'
        }, {
            name: 'koaStatic'
        }]
    }]);
}