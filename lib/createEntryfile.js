import fs from 'fs';
import path from 'path';
import ejs from 'ejs';
import prettier from 'prettier';


export const createEntryfile = (filename, options)=>{
   const code = fs.readFileSync(path.join('./template', filename+'.ejs'), 'utf-8');
   const processCode = ejs.render(code, options);
   return Promise.resolve(processCode);
}