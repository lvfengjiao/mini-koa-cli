**mini koa-template-cli
description: auto generate koa app and middlewares
## Usage

1. generate template
```sh
npm install mini-koa-template-cli --save-dev
mini-koa-template-cli
```

2. Answer questions
![Image text](https://bitbucket.org/lvfengjiao/mini-koa-cli/raw/58968f7955d2096937717f5f0793a69b720bbf40/screenshot/result.png)

3. Running test
![Image text](https://bitbucket.org/lvfengjiao/mini-koa-cli/raw/58968f7955d2096937717f5f0793a69b720bbf40/screenshot/result1.png)
![image text](https://bitbucket.org/lvfengjiao/mini-koa-cli/raw/58968f7955d2096937717f5f0793a69b720bbf40/screenshot/result2.png)